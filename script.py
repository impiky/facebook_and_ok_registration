import os
import selenium
import configparser
import pyodbc
import time
import random
import uuid
import json
import requests


from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions


DEFAULT_PWD = "123456QweW"

def getConfig(section, key, configfile):
    config = configparser.ConfigParser()
    config.read(configfile, encoding="utf-8")
    return config.get(section, key)
    

Driver = getConfig("database", "Driver", "config.ini")
SERVER = getConfig("database", "SERVER", "config.ini")
DATABASE = getConfig("database", "DATABASE", "config.ini")
UID = getConfig("database", "UID", "config.ini")
PWD = getConfig("database", "PWD", "config.ini")
yzmuser = getConfig("vcode", "username", "config.ini")
yzmpassword = getConfig("vcode", "password", "config.ini")
chromepath = getConfig("chrome", "chromedriverpath", "config.ini")
# vmid = getConfig("VMInfo", "VMID", VMCONFIG_FILE_PATH)
# internet_login = getConfig("internet", "login", "config.ini")
# internet_pwd = getConfig("internet", "password", "config.ini")
sbie_start = getConfig("sandboxie", "sbie_exe", "config.ini")


class DbManager:
    def __init__(self):
        self.driver = Driver
        self.server = SERVER
        self.database = DATABASE
        self.uid = UID
        self.pwd = PWD
        self.conn = None
        self.cur = None

    # 连接数据库
    # connect to database
    def connectDatabase(self):
        co = "DRIVER={};SERVER={};DATABASE={};UID={};PWD={}".format('{' + self.driver + '}', self.server,
                                                                    self.database, self.uid, self.pwd)
        try:
            self.conn = pyodbc.connect(co)
            self.cur = self.conn.cursor()
            return True
        except:
            print("Error:Can't connect to database")
            return False

    # 关闭数据库
    # close db
    def close(self):
        # 如果数据打开，则关闭；否则没有操作
        # close connection to db
        if self.conn and self.cur:
            self.cur.close()
            self.conn.close()
        return True

    def execute(self, sql, params=None, commit=False):
        # 连接数据库
        # connect to db
        res = self.connectDatabase()
        if not res:
            return False
        try:
            if self.conn and self.cur:
                # 正常逻辑，执行sql，提交操作
                # if all good execute sql query
                rowcount = self.cur.execute(sql)
                self.conn.commit()
                
        except:
            print("execute failed: " + sql)
            print("params: " + str(params))
            self.close()
            return False
        return rowcount

    def fetchone(self, sql, params=None):
        res = self.execute(sql, params)
        if not res:
            print("查询失败")
            return False
        result = self.cur.fetchone()
        self.close()
        return result
    
    def fetchall(self, sql, params=None):
        res = self.execute(sql)
        if not res:
            print("execute failed")
            return False
        results = self.cur.fetchall()
        # print("жџҐиЇўж€ђеЉџ" + str(results))
        self.close()
        return results


class Browser:

    def __init__(self, byid, useragent):
        self.options = ChromeOptions()
        self.options.add_argument('log-level=3')
        self.options.add_experimental_option('excludeSwitches', ['enable-automation'])
        self.options.add_argument('user-agent={}'.format(useragent))
        self.chromedata = "C:\\Sandbox\\alks\\" + str(byid) + "\\user\\current\\AppData\\Local\\Google\\Chrome\\User Data"
        self.options.add_argument(r"user-data-dir=" + self.chromedata)
        # self.options.add_argument("--headless")
        # self.options.add_argument("--start-maximized")
        self.chromepaths = sbie_start + " /box:" + str(byid) + " " + chromepath
        self.driver = webdriver.Chrome(executable_path=self.chromepaths, options=self.options)
        self.driver.maximize_window()
        self.wait = WebDriverWait(self.driver, 100)

    def close(self):
        self.driver.close()


def send_keys_delay_random(controller,keys,min_delay=0.05,max_delay=0.25):
    """
    send value key by key with delay
    """
    for key in keys:
        controller.send_keys(key)
        time.sleep(random.uniform(min_delay,max_delay))


def get_email_login_and_pwd(db_manager):
    """
    get all records which has registered email
    """
    return db_manager.fetchall("SELECT EmailJ, PasswordJ, BuyerID, BrowserUserAgent, ENFirstName, ENLastName FROM Buyer WHERE EmailJ IS NOT NULL AND EmailJ <> ' '")


def register_facebook(browser, first_name, last_name, email):
    browser.driver.get("https://www.facebook.com/")
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"content\"]/div/div/div/div")))
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"u_0_j\"]"), first_name)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"u_0_l\"]"), last_name)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"u_0_o\"]"), email)
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"u_0_q\"]")))
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"u_0_r\"]"), email)
    send_keys_delay_random(browser.driver.find_element_by_xpath("//*[@id=\"u_0_v\"]"), DEFAULT_PWD)
    browser.driver.find_element_by_xpath("//*[@id=\"year\"]/option[{}]".format(random.randint(1, 61))).click()
    browser.driver.find_element_by_xpath("//*[@id=\"u_0_z\"]/span[{}]".format(random.randint(1, 2))).click()
    browser.driver.find_element_by_xpath("//*[@id=\"u_0_11\"]").click()


def confirm_facebook(browser, email, password):
    browser.drive.get("https://e.mail.ru/inbox/")
    browser.wait.until(EC.visibility_of_element_located((By.XPATH, "//*[@id=\"root\"]/div/div[2]/div/div")))
    send_keys_delay_random(browser.driver.find_element_by_xpath(
        "//*[@id=\"root\"]/div/div[2]/div/div/div/form/div[2]/div[2]/div[1]/div/div/div/div/div/div[1]/div/input"), email)
    send_keys_delay_random(browser.driver.find_element_by_xpath(
        "//*[@id=\"root\"]/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div/div/div/div/input"), password)
    

db_manager = DbManager()
users_data = get_email_login_and_pwd(db_manager)
# print(users_data)
for user in range(0, len(users_data) - 1):
    browser = Browser(users_data[user][2], users_data[user][3])
    register_facebook(browser, users_data[user][4], users_data[user][5], users_data[user][0])
    confirm_facebook(browser, users_data[user][0], users_data[user][1])
    # print(users_data[user][2], users_data[user][3])
# print(user_data)

